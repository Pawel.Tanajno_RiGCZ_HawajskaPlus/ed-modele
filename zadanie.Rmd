---
title: "Lab"
author: "Krzysztof Rusek"
date: "1/11/2022"
output: html_document
---

#```{r setup, include=FALSE}
#knitr::opts_chunk$set(echo = TRUE)
#```


# SVM

Wytrenować SVM na danych o cenach domów. Cenę zamienić na zmienną kategoryczną $cena> 500000$.

Podzielic się na grupy i zbadac model

- Liniowy
- wielomianowy
- radilany

Dobrac parametry funcją `tune`


```{r}
# tutaj tak jak n
a labie działamy na zmiennej mówiącej czy mieszkanie jest droższe niż mediana, czy nie

# WAŻNE! Nie wiem czemu pierwsze odpalenie komórki czasami zwraca błąd, ale za kolejnym razem wszystko jest już w porządku

library(tidyverse)
library(modelr)
library(e1071)

# Wczytanie danych i stworzenie dwóch dataframe-ów - jeden z ceną a drugi z factorem czy czena jest powyżej mediany czy nie
mieszkania_bez_ceny <- mieszkania <- read_csv("mieszkania.csv", col_types = "nnffff")
mediana_ceny <- median(mieszkania$cena)
mieszkania_bez_ceny$drogie[mieszkania_bez_ceny$cena > mediana_ceny] <- 1
mieszkania_bez_ceny$drogie[mieszkania_bez_ceny$cena <= mediana_ceny] <- 0
mieszkania_bez_ceny <- mieszkania_bez_ceny %>% mutate("drogie" = as.factor(drogie))
mieszkania_bez_ceny$cena <- NULL


# trening modeli liniowych
linear_models <- tune.svm(drogie~.,
                   data=mieszkania_bez_ceny,
                   kernel='linear',
                   cost=c(1, 2, 4, 7, 10, 20, 30, 40, 50, 60, 80, 100))

cat("Cost value of best linear model: ", linear_models$best.model$cost, "\n")
cat("Best performance of linear model", linear_models$best.performance, "\n\n")


# trening modeli wielomianowych
polynomial_models <- tune.svm(drogie~.,
                   data=mieszkania_bez_ceny,
                   kernel='polynomial',
                   cost=c(50, 100, 200, 300, 400, 500, 600, 750, 900),
                   degree=c(1, 2, 3, 4, 5))

cat("Cost value of best polynomial model: ", polynomial_models$best.model$cost, "\n")
cat("Degree value of best polynomial model: ", polynomial_models$best.model$degree, "\n")
cat("Best performance of polynomial model", polynomial_models$best.performance, "\n\n")

if (polynomial_models$best.performance < linear_models$best.performance){
  best_model <- "polynomial"
  best_model.cost <- polynomial_models$best.model$cost
  best_model.degree <- polynomial_models$best.model$degree
  best_model.performance <- polynomial_models$best.performance
} else {
  best_model <- "linear"
  best_model.performance <- linear_models$best.performance
  best_model.cost <- linear_models$best.model$cost
}


# trening modeli radialnych
radial_models <- tune.svm(drogie~.,
                   data=mieszkania_bez_ceny,
                   kernel='radial',
                   cost=c(25, 50, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000),
                   gamma=c(0.4, 0.2, 0.1, 0.05, 0.02, 0.01))

cat("Cost value of best radial model: ", radial_models$best.model$cost, "\n")
cat("Gamma value of best radial model: ", radial_models$best.model$gamma, "\n")
cat("Best performance of radial model", radial_models$best.performance, "\n\n")

if (radial_models$best.performance < best_model.performance){
  best_model <- "radial"
  best_model.cost <- radial_models$best.model$cost
  best_model.gamma <- radial_models$best.model$gamma
  best_model.performance <- radial_models$best.performance
}
cat("--------------------------------------\n")
cat("Best model: ", best_model, "\n")
cat("Correctly classified : ", (1 - best_model.performance)*100, "% of households\n\n")
```

```{r}
# tu przewidujemy cenę zamiast factora tanio/drogo przy pomocy metody svm

# trening modeli liniowych
linear_models <- tune.svm(cena~.,
                   data=mieszkania,
                   kernel='linear',
                   cost=c(0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1, 2, 4, 8, 16, 32))

cat("Cost value of best linear model: ", linear_models$best.model$cost, "\n")
cat("Best performance of linear model", linear_models$best.performance, "\n\n")


# trening modeli wielomianowych
polynomial_models <- tune.svm(cena~.,
                              data=mieszkania, 
                              kernel='polynomial', 
                              cost=c(0.5, 1, 2, 4, 8, 16, 32, 48), 
                              degree=c(1, 2, 3, 4, 5),
                              tunecontrol=tune.control(cross=5),)

cat("Cost value of best polynomial model: ", polynomial_models$best.model$cost, "\n")
cat("Degree value of best polynomial model: ", polynomial_models$best.model$degree, "\n")
cat("Best performance of polynomial model", polynomial_models$best.performance, "\n\n")


if (polynomial_models$best.performance < linear_models$best.performance){
  best_model <- "polynomial"
  best_model.cost <- polynomial_models$best.model$cost
  best_model.degree <- polynomial_models$best.model$degree
  best_model.performance <- polynomial_models$best.performance
} else {
  best_model <- "linear"
  best_model.performance <- linear_models$best.performance
}


# trening modeli radialnych
radial_models <- tune.svm(cena~.,
                   data=mieszkania,
                   kernel='radial',
                   cost=c(10, 20, 30, 40, 50, 60, 80, 100, 200),
                   gamma=c(0.5, 0.2, 0.1, 0.05, 0.02, 0.01))

cat("Cost value of best radial model: ", radial_models$best.model$cost, "\n")
cat("Gamma value of best radial model: ", radial_models$best.model$gamma, "\n")
cat("Best performance of radial model", radial_models$best.performance, "\n\n")

if (radial_models$best.performance < best_model.performance){
  best_model <- "radial"
  best_model.cost <- radial_models$best.model$cost
  best_model.gamma <- radial_models$best.model$gamma
  best_model.performance <- radial_models$best.performance
}
cat("--------------------------------------\n")
cat("Best model: ", best_model, "\n")
cat("Mean error: ", sqrt(best_model.performance), " zł\n\n")
```


# Las losowy

- Porównac SVM z lasem losowym
- Wyznaczyć istotność cech
- Porównać skuteczność oob z walidacji krzyżowej 

```{r}
require(randomForest)
RF_models <- tune.randomForest(cena~.,
                               data = mieszkania,
                               mtry = c(1, 2, 3, 4),
                               nodesize = c(4, 8, 16, 32, 64),
                               ntree = c(500, 1000, 2000))

RF_best_model.performance <- RF_models$best.performance
cat("Mean error: ", sqrt(RF_best_model.performance), " zł\n")

# mtry nie może być wieksza niż liczba kolumn
cat("Number of variables randomly sampled as candidates at each split: ", RF_models$best.parameters$mtry, "\n")

cat("Node size: ", RF_models$best.parameters$nodesize, "\n")

# generalnie im więcej tym lepiej, z tym że wraz ze wzrostem liczby drzew rośnie nam złożoność obliczeniowa
cat("Number of trees to grow.", RF_models$best.parameters$ntree, "\n\n")

RF_models$best.model$importance

cat("\nThe most important factors determining household price are its area and location.\n\n\n")

barplot(c(sqrt(best_model.performance), sqrt(RF_best_model.performance)),
main = "Error value",
xlab = "",
ylab = "",
names.arg = c("SVM", "Random Forest"),
horiz = FALSE)

if (RF_best_model.performance < best_model.performance){
  cat("Random Forest performed better than SVM.")
} else{
  cat("SVM performed better than Random Forest.\n\n\n")
}

cat("It is important to notice that the amount of data was not big enough, so the result might not be correct.")

```
